# Storage Specification

`.abedit` files are all are structured like this: 
```c
struct SaveFile {
    uint32_t magic;
    uint32_t version;
    ... // other data
};
```

### Fields

| Name    | Description                                                                                                |
|:--------|:-----------------------------------------------------------------------------------------------------------|
| magic   | The magic number. This should *always* be equal to `0x21594147`.                                           |
| version | The version number. This will determine what the `data` is. The specifics, per version, can be seen below. |
| ...     | The data specific to each version, see below.                                                              |

Files are always in Little Endian byte order.

## Version 1

Version 1 represents all glyphs as a 10x10 grid of pixels, which are then scaled up to be seen and edited. Each pixel is represented by a bit, so the only values are 0 (unfilled), and 1 (filled). No colour value is stored in the file.

Storage files for Version 1 look something like this:
```c
struct V1 {
    uint32_t magic;
    uint32_t version;
    uint8_t length;
    struct Glyph data[length];
};

struct Glyph {
    char keysym;
    uint8_t data[13];
};
```

### Fields

`V1`:
| Name   | Description                                                                  |
|:-------|:-----------------------------------------------------------------------------|
| length | How many Glyphs are stored in the file. Must be between 1 and 94, inclusive. |
| data   | The Glyphs.                                                                  |

`Glyph`:
| Name   | Description                                                                                                                                                                                                                                                                                                   |
|:-------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| keysym | What key will have to be pressed in the editor to produce this glyph. The actual value can be between 0x20 (' ') and 0x7E ('~'), inclusive.                                                                                                                                                                   |
| data   | The actual 'pixels' that will be rendered as a character in the editor. The first 100 bits of these 104 bits are used. The last four bits must be all zeroes. These 100 bits represent the 10x10 grid---that is, the first 10 bits represent the top row of 10 pixels, the second the second-to-top row, etc. |

### Editable files

Editable files for Version One are ASCII files that look like this: 
```
2

:a:
O O O O O O O O O O
O O O O O O O O O O
O O O O O O O O O O
O O O X X O O O X O
O O X O O X O X X O
O X O O O O X X O O
O X O O O O X O O O
O X O O O O X X O O
O O X X X X O O X O
O O O O O O O O O O

:i:
O O O O O O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O X O O O O O
O O O O O O O O O O
```
The file starts out with the text number representing how many glyphs are in the file.

The definition for each glyph is separated by one empty line. The key that must be pressed to make that glyph is surrounded by semicolons and is on its own line. On the next 10 lines is the data. Each X or O represents a pixel, with an X representing a filled pixel and an O representing an empty one. The pixel definitions are each separated by a space, and the rows are separated by newlines.
