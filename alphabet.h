#ifndef ALPHABET_H_
#define ALPHABET_H_

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_stdinc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "util.h"

#define GLYPH_PARTS 10
#define GLYPH_SCALE 3
#define GLYPH_DIMENSION GLYPH_PARTS * GLYPH_SCALE

typedef struct {
    size_t left_crop;
    size_t right_crop;
    bool layout[GLYPH_PARTS][GLYPH_PARTS]; // Should be GLYPH_PARTS by GLYPH_PARTS
    bool initialised;
    SDL_Rect target;
    char keysym;
} Glyph;

typedef struct {
    uint8_t len; // Valid values
    Glyph* data;
} Alphabet;

static void check_char(char c, char* filename, const char* check_against) {
    if (check_against == NULL) {
        if (c < ' ' || c > '~') {
            fprintf(stderr, "Invalid character in file %s: `%c`", filename, c);
            exit(1);
        }
    } else {
        for (int i = 0; check_against[i] != '\0'; ++i) {
            if (c == check_against[i])
                return;
        }
        if (strlen(check_against) == 1) {
            fprintf(stderr, "Invalid character in file %s: `%c`! Expected `%s`!", filename, c, check_against);
        } else {
            fprintf(stderr, "Invalid character in file %s: `%c`! Expected one of `%s`!", filename, c, check_against);
        }
        exit(1);
    }
}

static Glyph slurp_glyph_from_str(const char* data, size_t len, size_t* idx, char* filename) {
    size_t i = *idx;
    while (i < len && isspace(data[i]))
        ++i;

    assert(len - i > 2);
    check_char(data[i++], filename, ":");
    check_char(data[i], filename, NULL);
    char sym = data[i++];
    check_char(data[i++], filename, ":");

    Glyph g = {0};
    g.keysym = sym;

    size_t covered = 0;
    size_t covered_this_line = 0;
    for (; i < len && covered < GLYPH_PARTS * GLYPH_PARTS; ++i) {
        while (i < len && isspace(data[i])) {
            if (data[i] == '\n') {
                if (covered_this_line < GLYPH_PARTS && covered_this_line > 0) {
                    fprintf(stderr, "Error parsing file %s: Expected %u items per line, found %lu!\n", filename, GLYPH_PARTS, covered_this_line);
                    exit(1);
                }
                covered_this_line = 0;
            }
            ++i;
        }
        if (i == len) {
            goto unfinished;
        }
        char c = data[i];
        fflush(stdout);
        check_char(c, filename, "XO");
        ((bool*) g.layout)[covered] = c == 'X';
        covered++;
        covered_this_line++;
    }
    if (covered != GLYPH_PARTS * GLYPH_PARTS) {
        goto unfinished;
    }
    ++i;
    *idx = i;
    return g;

unfinished:
    fprintf(stderr, "Error in file %s: Unfinished glyph!", filename);
    exit(1);
}

void print_glyph(FILE* stream, Glyph* to_print) {
    assert(to_print != NULL);

    const char* str1 = "(Glyph) { \n"
                       "    .layout = {\n";

    fputs(str1, stream);
    for (size_t y = 0; y < GLYPH_PARTS; ++y) {
        fputs("\t\t", stream);
        for (size_t x = 0; x < GLYPH_PARTS; ++x) {
            fputc(to_print->layout[y][x] ? 'X' : 'O', stream);
        }
        fputc('\n', stream);
    }
    fputs("\t},\n", stream);

    const char* str2 = "    .initialised = %s, \n"
                       "    .target = ... \n"
                       "    .keysym = '%c', \n"
                       "    .left_crop = %zu, \n"
                       "    .right_crop = %zu, \n"
                       "}; \n";

    fprintf(stream, str2, to_print->initialised ? "true" : "false", to_print->keysym, to_print->left_crop, to_print->right_crop);
}

Alphabet read_alphabet_from_text_file(char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        fprintf(stderr, "An error occurred opening file %s: %s", filename, strerror(errno));
        exit(1);
    }
    fseek(f, 0, SEEK_END);
    long raw_len = ftell(f);
    unsigned long len;

    if (raw_len < 0) {
        fprintf(stderr, "An error occurred using ftell on file %s: %s!\n", filename, strerror(errno));
        exit(1);
    } else {
        len = (unsigned long) raw_len;
    }
    fseek(f, 0, SEEK_SET);
    char* data = (char*) cmalloc(sizeof(char) * (len + 1));
    data[len] = '\0';
    cfread(data, sizeof(char), len, f, filename);

    char* endptr = NULL;
    long raw_num_glyphs = strtol(data, &endptr, 10);
    if (endptr == data || !isspace(*endptr)) {
        fprintf(stderr, "Invalid definition file: invalid number!\n");
        exit(1);
    }

    if (raw_num_glyphs > UINT8_MAX) {
        fprintf(stderr, "Error processing file %s: Too many glyphs! Found %zu, max %i", filename, raw_num_glyphs, UINT8_MAX);
        exit(1);
    } else if (raw_num_glyphs < 0) {
        fprintf(stderr, "Error processing file %s: Cannot have negative glyphs (found %li)!\n", filename, raw_num_glyphs);
        exit(1);
    }
    assert(SIZE_MAX / raw_num_glyphs >= len && "Len is too large! Overflow may occur!");
    uint8_t num_glyphs = (uint8_t) raw_num_glyphs;
    Glyph* glyphs = (Glyph*) cmalloc(sizeof(Glyph) * num_glyphs);

    size_t idx = endptr - data;
    for (size_t i = 0; i < num_glyphs; ++i) {
        if (idx >= len) {
            fprintf(stderr, "Error in file %s: Expected %i glyphs, found %zu!", filename, num_glyphs, i);
            exit(1);
        }
        Glyph maybe = slurp_glyph_from_str(data, len, &idx, filename);
        for (size_t j = 0; j < i; ++j) {
            if (glyphs[j].keysym == maybe.keysym) {
                fprintf(stderr, "Error processing file %s: Two glyphs cannot have the same keysym! (They both have `%c`!)", filename, maybe.keysym);
                exit(1);
            }
        }
        glyphs[i] = maybe;
    }

    for (; idx < len; idx++) {
        if (!isspace(data[idx])) {
            fprintf(stderr, "Error in file %s: Unexpected extra character after finishing: `%c`!\n", filename, data[idx]);
            exit(1);
        }
    }

    free(data);
    return (Alphabet) {
        .len = (uint8_t) num_glyphs,
        .data = glyphs,
    };
}

// TODO: Error handling
Alphabet read_alphabet_from_file_v1(FILE* f, char* filename) {

    uint8_t len;
    cfread(&len, sizeof(len), 1, f, filename);
    assert(len > 0 && "not enough glyphs in file");
    assert(len < 95 && "too many glyphs in file");

    Glyph* data = (Glyph*) cmalloc(sizeof(Glyph) * len);

    for (int i = 0; i < len; ++i) {

        char sym;
        cfread(&sym, sizeof(sym), 1, f, filename);
        assert(sym >= ' ' && "unknown character");
        assert(sym <= '~' && "unknown character");

        uint8_t raw_data[13];
        cfread(&raw_data, sizeof(uint8_t), 13, f, filename);

        for (int i = 0; i < 13; ++i) {
        }

        Glyph g = {0};
        g.initialised = false;
        g.keysym = sym;

        bool* layout = (bool*) g.layout;

        for (int i = 0; i < GLYPH_PARTS * GLYPH_PARTS; ++i) {
            uint8_t byte = raw_data[i / 8];
            bool bit = ((byte >> (i % 8)) & 1) == 1;
            layout[i] = bit;
        }

        data[i] = g;
    }
    return (Alphabet) {
        .len = len,
        .data = data
    };
}

const uint32_t MAGIC = 0x21594147;

bool alphabet_eq(Alphabet* a, Alphabet* b) {
    return (a->len == b->len) && (memcmp(a->data, b->data, a->len) == 0);
}

void write_alphabet_to_file(const Alphabet* a, char* filename) {
    FILE* f = fopen(filename, "wb");

    cfwrite(&MAGIC, sizeof(MAGIC), 1, f, filename);

    uint32_t version = 1;
    cfwrite(&version, sizeof(version), 1, f, filename);

    cfwrite(&a->len, sizeof(a->len), 1, f, filename);

    for (int i = 0; i < a->len; ++i) {
        cfwrite(&a->data[i].keysym, sizeof(char), 1, f, filename);
        uint8_t data[13] = {0};
        bool* layout = (bool*) a->data[i].layout;
        for (int i = 0; i < GLYPH_PARTS * GLYPH_PARTS; ++i) {
            if (layout[i]) {
                uint8_t* byte = &data[i / 8];
                *byte |= 128 >> (i % 8);
            }
        }
        cfwrite(data, sizeof(uint8_t), 13, f, filename);
    }
    fclose(f);
}

#define X true
#define O false

Glyph default_alphabet[] = {(Glyph){
                        .layout = {
                            {O, O, O, O, O, O, O, O, O, O},
                            {O, O, O, O, O, O, O, O, O, O},
                            {O, O, O, O, O, O, O, O, O, O},
                            {O, O, O, X, X, O, O, O, X, O},
                            {O, O, X, O, O, X, O, X, X, O},
                            {O, X, O, O, O, O, X, X, O, O},
                            {O, X, O, O, O, O, X, O, O, O},
                            {O, X, O, O, O, O, X, X, O, O},
                            {O, O, X, X, X, X, O, O, X, O},
                            {O, O, O, O, O, O, O, O, O, O},
                        },
                        .initialised = false,
                        .target = {0},
                        .keysym = 'a',
                    },
};
int default_alphabet_len = sizeof(default_alphabet) / sizeof(default_alphabet[0]);
#undef X
#undef O


#endif // ALPHABET_H_
