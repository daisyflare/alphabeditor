#ifndef UTIL_H_
#define UTIL_H_

#include <SDL2/SDL_error.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_stdinc.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#define UNPACK_COLOUR(c) (c >> 8 * 3) & 0xFF, (c >> 8 * 2) & 0xFF, (c >> 8 * 1) & 0xFF, (c >> 8 * 0) & 0xFF

#define BACKGROUND 0x000000FF
#define FOREGROUND 0xFFFFFFFF


void* checkp(void* v, const char* msg) {
    if (v == NULL) {
        fprintf(stderr, "Error doing %s: %s\n", msg, SDL_GetError());
        exit(1);
    }
    return v;
}

void checki(int v, const char* msg) {
    if (v != 0) {
        fprintf(stderr, "Error doing %s: %s\n", msg, SDL_GetError());
        exit(1);
    }
}

void* cstdp(void* v, const char* msg) {
    if (v == NULL) {
        fprintf(stderr, "Error doing %s: %s\n", msg, strerror(errno));
        exit(1);
    }
    return v;
}

void cstdi(int v, const char* msg) {
    if (v != 0) {
        fprintf(stderr, "Error doing %s: %s\n", msg, strerror(errno));
        exit(1);
    }
}

int cfread(void* p, size_t s, size_t n, FILE* f, char* filename) {
    if (f == NULL)
        goto error;

    {
        size_t read = fread(p, s, n, f);
        if (ferror(f) || read < n) {
            fprintf(stderr, "%zu, %zu", read, n);
            goto error;
        }
        return read;
    }

error:
    fprintf(stderr, "Error reading file %s: %s\n", filename, strerror(errno));
    exit(1);
}

void* cmalloc(size_t s) {
    void* a = malloc(s);
    if (a == NULL) {
        fprintf(stderr, "Could not allocate memory: %s!", strerror(errno));
        exit(1);
    }
    return a;
}

void* crealloc(void* p, size_t s) {
    void* a = realloc(p, s);

    if (a == NULL) {
        fprintf(stderr, "Could not reallocate memory: %s!", strerror(errno));
        exit(1);
    }

    return a;
}

int cfwrite(const void* p, size_t s, size_t n, FILE* f, char* filename) {
    if (f == NULL)
        goto error;

    {
        size_t written = fwrite(p, s, n, f);
        if (ferror(f) || written < n) {
            goto error;
        }
        return written;
    }

error:
    fprintf(stderr, "Error writing to file %s: %s\n", filename, strerror(errno));
    exit(1);
}


#endif // UTIL_H_
