CFLAGS=-Wall -Wextra -lSDL2 -ggdb -std=c11 -Wno-odr -Wno-address
CC=gcc

.PHONY: all
all: abe writer

abe: main.c util.h alphabet.h
	$(CC) $(CFLAGS) -o abe main.c alphabet.h util.h

writer: writer.c alphabet.h util.h
	$(CC) $(CFLAGS) -o writer writer.c alphabet.h util.h
