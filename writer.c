#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_render.h>
#include <stdint.h>
#include <stdio.h>
#include "alphabet.h"
#include "util.h"
#include <SDL2/SDL.h>

typedef struct {
    bool filled[GLYPH_PARTS][GLYPH_PARTS];
} Gui;

Gui gui = {0};

#define WIDTH 800
#define HEIGHT WIDTH

void clear_gui(Gui* gui) {
    memset(gui->filled, 0, sizeof(gui->filled));
}

void output_gui(Gui* gui) {
    puts("::\n");
    for (size_t y = 0; y < GLYPH_PARTS; ++y) {
        for (size_t x = 0; x < GLYPH_PARTS; ++x) {
            putchar(gui->filled[y][x] ? 'X' : 'O');
            putchar(' ');
        }
        putchar('\n');
    }
    fflush(stdout);
}

typedef enum {
    DIR_UP,
    DIR_DOWN,
    DIR_LEFT,
    DIR_RIGHT
} Direction;

void shift_gui(Gui* gui, Direction direction) {
    size_t x_shift = 0;
    size_t y_shift = 0;
    switch (direction) {
        case DIR_UP:
            y_shift = GLYPH_PARTS - 1;
            break;
        case DIR_DOWN:
            y_shift = 1;
            break;
        case DIR_LEFT:
            x_shift = GLYPH_PARTS - 1;
            break;
        case DIR_RIGHT:
            x_shift = 1;
            break;
    }
    bool extra_data[GLYPH_PARTS][GLYPH_PARTS] = {0};
    memcpy(extra_data, gui->filled, sizeof(extra_data));
    for (size_t y = 0; y < GLYPH_PARTS; ++y) {
        for (size_t x = 0; x < GLYPH_PARTS; ++x) {
            gui->filled[(y + y_shift) % GLYPH_PARTS][(x + x_shift) % GLYPH_PARTS] = extra_data[y][x];
        }
    }
}

bool update(const SDL_Event* e, SDL_Renderer* r, bool* do_render) {
    switch (e->type) {
        case SDL_MOUSEBUTTONDOWN: {
            Sint32 raw_x = e->button.x;
            Sint32 raw_y = e->button.y;

            if (0 < raw_x && raw_x < WIDTH && 0 < raw_y && raw_y < HEIGHT) {
                size_t x = (size_t) ((double) GLYPH_PARTS * ((double) raw_x / (double) WIDTH));
                size_t y = (size_t) ((double) GLYPH_PARTS * ((double) raw_y / (double) HEIGHT));
                gui.filled[y][x] = !gui.filled[y][x];
            }
            *do_render = true;
        }
            break;
        case SDL_KEYDOWN:
            switch (e->key.keysym.sym) {
                case SDLK_c:
                    clear_gui(&gui);
                    *do_render = true;
                    break;
                case SDLK_RETURN:
                    output_gui(&gui);
                    break;
                case SDLK_RETURN2:
                    output_gui(&gui);
                    break;
                case SDLK_KP_ENTER:
                    output_gui(&gui);
                    break;
                case SDLK_UP:
                    shift_gui(&gui, DIR_UP);
                    *do_render = true;
                    break;
                case SDLK_LEFT:
                    shift_gui(&gui, DIR_LEFT);
                    *do_render = true;
                    break;
                case SDLK_RIGHT:
                    shift_gui(&gui, DIR_RIGHT);
                    *do_render = true;
                    break;
                case SDLK_DOWN:
                    shift_gui(&gui, DIR_DOWN);
                    *do_render = true;
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return true;
}

void render(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(BACKGROUND));
    SDL_RenderClear(r);
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(FOREGROUND));

    for (size_t i = 0; i < GLYPH_PARTS - 1; ++i) {
        int x = (i + 1) * WIDTH / GLYPH_PARTS;
        int y = (i + 1) * HEIGHT / GLYPH_PARTS;

        SDL_RenderDrawLine(r, x, 0, x, HEIGHT);

        SDL_RenderDrawLine(r, 0, y, WIDTH, y);

    }

    for (size_t y = 0; y < GLYPH_PARTS; ++y) {
        for (size_t x = 0; x < GLYPH_PARTS; ++x) {
            if (gui.filled[y][x]) {
                SDL_Rect rect = (SDL_Rect) {
                    .h = HEIGHT / GLYPH_PARTS,
                    .w = WIDTH / GLYPH_PARTS,
                    .x = (WIDTH / GLYPH_PARTS) * x,
                    .y = (HEIGHT / GLYPH_PARTS) * y,
                };
                SDL_RenderFillRect(r, &rect);
            }
        }
    }

    SDL_RenderPresent(r);
}

int main() {
    checki(SDL_Init(SDL_INIT_VIDEO), "Initiating SDL");
    SDL_Window *w = checkp(SDL_CreateWindow("Editor", 0, 0, WIDTH, HEIGHT, 0),
                         "opening window");

    SDL_Renderer *r = checkp(SDL_CreateRenderer(w, -1, SDL_RENDERER_ACCELERATED),
                           "creating renderer");

    render(r);

    bool running = true;

    while (running) {
        SDL_Event e;
        SDL_WaitEvent(&e);
        switch (e.type) {
            case SDL_QUIT:
                running = false;
                break;
            default:
            {
                bool do_render = false;
                running = update(&e, r, &do_render);
                if (do_render) {
                    render(r);
                }
            }; break;
        }
    }

    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    SDL_Quit();

    return 0;
}
