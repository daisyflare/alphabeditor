#include <SDL2/SDL_error.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_stdinc.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_video.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <assert.h>
#include <stdbool.h>

#include "alphabet.h"
#include "util.h"

size_t window_width =  800;
size_t window_height = 600;

typedef struct {
    enum {
        Newline,
        Space,
        Char
    } typ;
    size_t idx;
} glyph_index_t;

typedef struct {
    glyph_index_t* data;
    size_t alloced;
    size_t len;
} Buffer;

Buffer buf = (Buffer) {
    .data = NULL,
    .alloced = 0,
    .len = 0,
};

#define FLICKER_THRESHOLD 750
typedef struct {
    size_t buffer_idx;
    Uint64 last_hit;
} Cursor;

Cursor cursor = (Cursor) {
    .buffer_idx = 0,
    .last_hit = 0,
};

typedef struct {
    char sym;
    glyph_index_t action;
} Mapping;

typedef struct {
    Mapping* data;
    size_t alloced;
    size_t len;
} MapList;

MapList map_list = (MapList) {
    .data = NULL,
    .alloced = 0,
    .len = 0,
};

Alphabet alphabet = (Alphabet) {
    .len = 0,
    .data = NULL,
};

SDL_Texture* alphabet_texmap = NULL;

void render_glyph(SDL_Renderer* r, const size_t* raw_idx, int x, int y, bool is_cursor) {
    if (raw_idx == NULL && is_cursor) {
        SDL_Rect cursor_rect = (SDL_Rect) {
            .h = GLYPH_DIMENSION,
            .w = GLYPH_DIMENSION,
            .x = x,
            .y = y,
        };
        SDL_RenderFillRect(r, &cursor_rect);
    } else {
        size_t idx = *raw_idx;
        assert(idx < alphabet.len);
        Glyph* ch = &alphabet.data[idx];
        assert(ch->initialised);
        SDL_Rect src_rect = (SDL_Rect) {
            .h = GLYPH_DIMENSION,
            .w = GLYPH_DIMENSION - ch->right_crop * GLYPH_SCALE,
            .x = GLYPH_DIMENSION * idx + ch->left_crop * GLYPH_SCALE,
            .y = is_cursor ? GLYPH_DIMENSION : 0,
        };
        SDL_Rect dst_rect = (SDL_Rect) {
            .h = GLYPH_DIMENSION,
            .w = GLYPH_DIMENSION,
            .x = x,
            .y = y,
        };
        checki(SDL_RenderCopy(r, alphabet_texmap, &src_rect, &dst_rect), "rendering glyph");
    }
}

size_t minsize(size_t a, size_t b) {
    return a > b ? b : a;
}

void initialise_glyph(SDL_Renderer* r, int idx) {
    assert(idx < alphabet.len);
    Glyph* ch = &alphabet.data[idx];
    if (ch->initialised) {
        return;
    }
    int glyph_offset = GLYPH_DIMENSION * idx;

    int num_filled = 0;
    for (int y = 0; y < GLYPH_PARTS; ++y) {
        for (int x = 0; x < GLYPH_PARTS; ++x) {
            if (ch->layout[y][x] != 0) {
                num_filled++;
            }
        }
    }

    SDL_Rect *rects =
        checkp(SDL_malloc(sizeof(SDL_Rect) * num_filled), "malloc rects");

    size_t left_crop = GLYPH_PARTS;
    size_t right_crop = GLYPH_PARTS;

    int index = 0;
    for (int y = 0; y < GLYPH_PARTS; ++y) {
        for (int x = 0; x < GLYPH_PARTS; ++x) {
            if (ch->layout[y][x] != 0) {

                left_crop = minsize(left_crop, x);
                right_crop = minsize(right_crop, GLYPH_PARTS - x);

                rects[index].h = GLYPH_SCALE;
                rects[index].w = GLYPH_SCALE;
                rects[index].x = glyph_offset + x * GLYPH_SCALE;
                rects[index].y = y * GLYPH_SCALE;

                index++;
            }
        }
    }
    ch->left_crop = left_crop;
    ch->right_crop = right_crop;
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(FOREGROUND));
    checki(SDL_RenderFillRects(r, rects, num_filled), "filling glyph rectangles");

    index = 0;
    for (int y = 0; y < GLYPH_PARTS; ++y) {
        for (int x = 0; x < GLYPH_PARTS; ++x) {
            if (ch->layout[y][x] != 0) {
                rects[index].y += GLYPH_DIMENSION;

                index++;
            }
        }
    }

    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(BACKGROUND));
    checki(SDL_RenderFillRects(r, rects, num_filled), "filling glyph rectangles");

    SDL_free(rects);
    ch->initialised = true;
}

#define BUFFER_CHUNK_SIZE 10
void buffer_push(Buffer* b, glyph_index_t idx) {
    if (b->alloced == b->len) {
        b->alloced += BUFFER_CHUNK_SIZE * sizeof(glyph_index_t);
        b->data = crealloc(b->data, sizeof(*b->data) * b->alloced);
    }

    b->data[b->len++] = idx;
}

void buffer_pop(Buffer* b) {
    if (b->len != 0)
        b->len--;
    return;
}

void destroy_alphabet() {
    SDL_DestroyTexture(alphabet_texmap);
    if (alphabet.data != default_alphabet && alphabet.data != NULL) {
        free(alphabet.data);
        alphabet.len = 0;
    }
    if (map_list.data != NULL) {
        free(map_list.data);
        map_list.alloced = 0;
        map_list.len = 0;
    }
}

void maplist_push(MapList* m, Mapping to_push) {
    if (m->alloced == m->len && m->data != NULL && m->alloced != 0) {
        m->alloced += BUFFER_CHUNK_SIZE * sizeof(glyph_index_t);
        m->data = crealloc(m->data, sizeof(*m->data) * m->alloced);
    } else if (m->data == NULL || m->alloced == 0) {
        m->alloced += BUFFER_CHUNK_SIZE * sizeof(glyph_index_t);
        m->data = cmalloc(sizeof(*m->data) * m->alloced);
    }

    m->data[m->len++] = to_push;
}

void initialise_mapping(MapList* m, size_t idx) {
    assert(idx < alphabet.len);
    assert(alphabet.data != NULL);
    Mapping mapping = (Mapping) {
        .sym = alphabet.data[idx].keysym,
        .action = (glyph_index_t) {
            .idx = idx,
            .typ = Char
        }
    };
    maplist_push(m, mapping);
}

void print_mapping(FILE* stream, Mapping* m) {

    char* fmt = "(Mapping) {\n"
                "    .sym = '%c',\n"
                "    .action = (glyph_index_t) {\n"
                "        .idx = %zu,\n"
                "        .typ = %s\n"
                "    }\n"
                "};\n";
    fprintf(stream, fmt, m->sym, m->action.idx, m->action.typ == Space ? "Space" : m->action.typ == Newline ? "Newline" : "Char");
}

void initialise_alphabet(SDL_Renderer* r) {
    alphabet_texmap = checkp(SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888,
                                            SDL_TEXTUREACCESS_TARGET,
                                            GLYPH_DIMENSION * alphabet.len,
                                            GLYPH_DIMENSION * 2),
                          "creating alphabet texture map");

    Mapping space = (Mapping) {
        .sym = ' ',
        .action = (glyph_index_t) {
            .idx = 0,
            .typ = Space
        }
    };

    Mapping enter = (Mapping) {
        .sym = '\n',
        .action = (glyph_index_t) {
            .idx = 0,
            .typ = Newline,
        }
    };

    Mapping enter2 = (Mapping) {
        .sym = '\r',
        .action = (glyph_index_t) {
            .idx = 0,
            .typ = Newline,
        }
    };

    maplist_push(&map_list, space);
    maplist_push(&map_list, enter);
    maplist_push(&map_list, enter2);

    checki(SDL_SetRenderTarget(r, alphabet_texmap), "targeting alphabet_texmap");

    checki(SDL_SetRenderDrawColor(r, UNPACK_COLOUR(FOREGROUND)), "setting draw colour to foreground");
    SDL_Rect background = (SDL_Rect) {
        .h = GLYPH_DIMENSION,
        .w = GLYPH_DIMENSION * alphabet.len,
        .x = 0,
        .y = GLYPH_DIMENSION,
    };

    checki(SDL_RenderFillRect(r, &background), "filling background rectangle on alphabet texturemap");

    for (int i = 0; i < alphabet.len; ++i) {
        initialise_glyph(r, i);
        assert(alphabet.data[i].initialised);
        initialise_mapping(&map_list, i);
    }

    SDL_RenderPresent(r);

    checki(SDL_SetRenderTarget(r, NULL), "targeting whole window");
}

void cursor_reset_time(Cursor* c) {
    c->last_hit = SDL_GetTicks64();
}

void handle_key_press(char ch) {
    int numkeys = 0;
    const Uint8* state = SDL_GetKeyboardState(&numkeys);
    bool excl = (state[SDL_SCANCODE_LSHIFT] == 1 || state[SDL_SCANCODE_RSHIFT] == 1) && ch == '1';
    if (excl)
        ch = '!';
    bool newl = ch == '\n' || ch == '\r';
    if (' ' <= ch || ch <= '~' || newl) {
        for (size_t i = 0 ; i < map_list.len; ++i) {
            if(map_list.data[i].sym == ch) {
                buffer_push(&buf, map_list.data[i].action);
                cursor.buffer_idx++;
                cursor_reset_time(&cursor);
                break;
            }
        }
    }
}

char* alphabet_source = "example/greek.txt";

bool update(SDL_Event* e, SDL_Renderer* r, bool* do_render) {
    switch (e->type) {
        case SDL_WINDOWEVENT:
            switch (e->window.type) {
                case SDL_WINDOWEVENT_RESIZED:
                    assert(e->window.data1 > 0);
                    assert(e->window.data2 > 0);
                    window_width = e->window.data1;
                    window_height = e->window.data2;
                    break;
                default:
                    break;
            }
            *do_render = true;
            break;
        case SDL_KEYDOWN: {

            }
            char ch = (char) e->key.keysym.sym;
            switch (e->key.keysym.sym) {
                case SDLK_r: {
                    int numkeys = 0;
                    const Uint8* state = SDL_GetKeyboardState(&numkeys);
                    if (state[SDL_SCANCODE_LCTRL] == 1 || state[SDL_SCANCODE_RCTRL] == 1) {
                        destroy_alphabet();
                        alphabet = read_alphabet_from_text_file(alphabet_source);
                        /* for (size_t i = 0; i < alphabet.len; ++i) { */
                        /*     print_glyph(stdout, &alphabet.data[i]); */
                        /* } */
                        initialise_alphabet(r);
                        /* for (size_t i = 0; i < map_list.len; ++i) { */
                        /*     print_mapping(stdout, &map_list.data[i]); */
                        /* } */
                        /* printf("--------------------\n"); */
                        break;
                    }
                    handle_key_press(ch);
                    break;
                }
                case SDLK_BACKSPACE:
                    buffer_pop(&buf);
                    cursor_reset_time(&cursor);
                    break;
                case SDLK_ESCAPE:
                    return false;
                default:
                    handle_key_press(ch);
                    break;
            }
            *do_render = true;
            break;
        default:
            break;
    }

    return true;
}

size_t glyph_width(Glyph* g) {
    return (GLYPH_PARTS - g->left_crop - g->right_crop) * GLYPH_SCALE;
}

void render(SDL_Renderer* r, bool show_cursor) {
    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(BACKGROUND));
    SDL_RenderClear(r);

    SDL_SetRenderDrawColor(r, UNPACK_COLOUR(FOREGROUND));

    int line = 0;
    int x_offset = 0;
    #define PADDED_DISTANCE (GLYPH_DIMENSION + GLYPH_SCALE)
    for (size_t i = 0; i < buf.len; ++i) {
        switch (buf.data[i].typ) {
            case Newline:
                line++;
                x_offset = 0;
                continue;
            case Space:
                x_offset += GLYPH_DIMENSION;
                continue;
            default:
                break;
        }
        size_t idx = buf.data[i].idx;
        render_glyph(r, &idx,  x_offset, PADDED_DISTANCE * line, cursor.buffer_idx == i && show_cursor);
        x_offset += glyph_width(&alphabet.data[idx]) + GLYPH_SCALE * 5;
    }

    if (cursor.buffer_idx >= buf.len && show_cursor) {
        render_glyph(r, NULL, x_offset, PADDED_DISTANCE * line, true);
    }

    SDL_RenderPresent(r);
}

void process_next_arg(int* argc, char*** argv, bool act) {
    assert(*argc > 0);

    if (act) {
        alphabet_source = (*argv)[0];
    }

    *argc = *argc - 1;
    *argv = *argv + 1;
}

int main(int argc, char** argv) {

    process_next_arg(&argc, &argv, false); // get rid of program name
    while (argc > 0) {
        process_next_arg(&argc, &argv, true);
    }

    checki(SDL_Init(SDL_INIT_VIDEO), "Initiating SDL");

    alphabet.data = default_alphabet;
    alphabet.len = default_alphabet_len;

    SDL_Window *w = checkp(SDL_CreateWindow("Editor", 0, 0, window_width, window_height, SDL_WINDOW_RESIZABLE),
                         "opening window");

    SDL_Renderer *r = checkp(SDL_CreateRenderer(w, -1, SDL_RENDERER_ACCELERATED),
                           "creating renderer");

    initialise_alphabet(r);

    render(r, true);

    bool running = true;

    while (running) {
        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
                case SDL_QUIT:
                    running = false;
                    break;
                default:
                {
                    bool do_render = false;
                    running = update(&e, r, &do_render);
                    if (do_render) {
                        render(r, true);
                    }
                }; break;
            }
        }
        Uint64 cur_time = SDL_GetTicks64();
        render(r, (cur_time - cursor.last_hit) % (FLICKER_THRESHOLD * 2) <= FLICKER_THRESHOLD);
        SDL_Delay(25);
    }

    SDL_DestroyTexture(alphabet_texmap);
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    SDL_Quit();

    return 0;
}
